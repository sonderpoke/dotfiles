#!/bin/bash

#----------------------------------- EXAMPLE ------------------------------------#
# $HOME/.config/tmux/init.sh \
#   --basedir "$HOME/work/KCIS2/vlkcis2-events" \
#   --mux "events-backend" \
#   --mux "events-frontend" \
#   --dev_env_path "$HOME/work/KCIS2/dev_env/" \
#   --dev_env_args "keycloak,postgres,proxy_server" \
#--------------------------------------------------------------------------------#

cmd=

function init_tmux() {
  basedir=$1
  project=$(echo $1 | sed -e 's/\//\ /g' | awk '{print $NF}')
  tmux new -s $project -c "${basedir}" -d
}

function setup_dev_env() {
  array=(${1//,/ })

  if [[ ${#array[@]} > 0 ]]; then
    tmux neww -t $project: -n servers
  fi

  if [[ ${#array[@]} > 1 ]]; then
    tmux splitw -t $project:servers -v
  fi

  if [[ ${#array[@]} > 2 ]]; then
    tmux splitw -t $project:servers.0 -h
  fi

  index=0


  for i in "${!array[@]}"
  do
    case "${array[i]}" in
      'keycloak')
        tmux send -t $project:servers.${index} "cd ${dev_env_path}/keycloak/keycloak-15.0.2/bin"
        tmux send -t $project:servers.${index} "./standalone.sh"
        ((index++))
        ;;
      'postgres')
        tmux send -t $project:servers.${index} "cd ${dev_env_path}/postgres"
        tmux send -t $project:servers.${index} "./start.sh"
        ((index++))
        ;;
      'proxy_server')
        tmux send -t $project:servers.${index} "cd $dev_env_path/proxy_server"
        tmux send -t $project:servers.${index} "node server.js"
        ((index++))
        ;;
    esac
  done
}

function add_mux() {
  tmux neww -t $project: -n $1
  tmux splitw -t $project:$1 -v
  tmux send -t $project:$1.0 "cd ${basedir}/$1 && nvim "

  if [[ $(ls "$basedir/$1" | grep package.json) != "" ]]; then
    mux_cmd="ng s"
  else
    mux_cmd="mvn clean spring-boot:run"
  fi

  tmux send -t $project:$1.1 "cd ${basedir}/$1"
  tmux send -t $project:$1.1 "$mux_cmd"
}

function parse_command_arg() {
  if [[ $cmd = "" ]]; then
    echo "argument without command"
  else
    case $cmd in
      '--basedir')
        init_tmux $1
        ;;
      '--dev_env_path')
        dev_env_path=$1
        ;;
      '--dev_env_args')
        setup_dev_env $1
        ;;
      '--mux')
        add_mux $1
        ;;
    esac
  fi

  cmd=
}

function parse_command() {
  cmd=$1
}

function parse_argument() {
  case $1 in
    --*)
      parse_command $1
      ;;
    [^-]*)
      parse_command_arg $1
      ;;
  esac
}


for var in "$@"
do
  parse_argument "$var"
done

if [[ $project != "" ]]; then
  tmux attach-session -t $project
fi
