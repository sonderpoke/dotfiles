#!/usr/bin/env bash

eais=`find ~/work/EAIS2 -maxdepth 1 -mindepth 1 -type d`
work=`find ~/work -maxdepth 1 -mindepth 1 -type d`
kcis=`find ~/work/KCIS2 -maxdepth 1 -mindepth 1 -type d`
personal=`find ~/personal -maxdepth 1 -mindepth 1 -type d`

selected=`echo -e "$eais\n$work\n$personal\n$kcis" | fzf --query=$1`

if [[ ! -z $selected ]]; then
  dirname=`basename $selected`
  `tmux -u attach-session -t $dirname || tmux -u new -s $dirname -c $selected`
fi
