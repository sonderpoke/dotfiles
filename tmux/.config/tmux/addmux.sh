#!/usr/bin/env bash

dir=`pwd`
items=`find $dir -maxdepth 1 -mindepth 1 -type d ! -path '*/.git'`

project=`basename $dir`
selected=`echo "$items" | fzf --query=$1`
# selected=`echo "$items" | rofi -dmenu -p "add window to $project:"`
#selected=`echo "$items" | rofi -dmenu -p "add window to $project"`

if [[ ! -z $selected ]]; then
  dirname=`basename $selected`
  tmux neww -t $project: -n $dirname "cd $selected && nvim"
  tmux splitw -t $project:$dirname -v
  tmux send -t $project:$dirname.1 "cd $dirname"
fi
