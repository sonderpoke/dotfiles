#!/bin/bash
echo $(sudo ls /var/cache/pacman/pkg/ | wc -l) packages in cache
echo $(du -sh /var/cache/pacman/pkg/) in storage space

sudo pacman -Sy --needed
sudo pacman --needed --noconfirm -S archlinux-keyring
sudo pacman -Syu --needed

