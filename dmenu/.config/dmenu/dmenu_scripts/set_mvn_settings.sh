#!/bin/bash

declare -a options=(
"default"
"default_w_comments"
"asseco"
)

maven_settings_dir_path="$HOME/dev/maven/apache-maven-3.8.3/conf"

choice=$(printf '%s\n' "${options[@]}" | dmenu -i -p 'maven settings')

case $choice in
  'default')
    cp "$maven_settings_dir_path/settings_default.xml" "$maven_settings_dir_path/settings.xml"
    ;;
  'default_w_comments')
    cp "$maven_settings_dir_path/settings_default_w_comments.xml" "$maven_settings_dir_path/settings.xml"
    ;;
  'asseco')
    cp "$maven_settings_dir_path/settings_asseco.xml" "$maven_settings_dir_path/settings.xml"
    ;;
esac

