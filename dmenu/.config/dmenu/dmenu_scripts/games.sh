#!/bin/bash

declare -a options=(
"dota"
"minecraft"
"valheim"
"trackmania"
"tibia"
)

choice=$(printf '%s\n' "${options[@]}" | dmenu -i -l 10 -p 'Choose workspace to load')

case $choice in
  'dota')
    $(steam steam://rungameid/570)
    ;;
  'valheim')
    $(steam steam://rungameid/892970)
    ;;
  'minecraft')
    $(minecraft-launcher)
    ;;
  'trackmania')
    $(steam steam://rungameid/375900)
    ;;
  'tibia')
    /home/czr/Games/Tibia/start-tibia-launcher.sh
    ;;
esac

