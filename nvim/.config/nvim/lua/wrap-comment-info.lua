local M = {}

local function getCommentTuple()
  local filetype = vim.api.nvim_buf_get_option(vim.api.nvim_win_get_buf(0), 'filetype')
  if filetype == 'java' or
     filetype == 'javascript' or
     filetype == 'scss' or
     filetype == 'typescript' then
    return '/*', '*/'
  end
  if filetype == 'html' then
    return '<!--', '-->'
  end
  if filetype == 'lua' or
     filetype == 'sql' then
    return '--', '--'
  end
  if filetype == 'vimwiki' then
    return '%%', '%%'
  end
  return '#', '#'
end

local function prompt(prompt_text)
  local input = vim.fn.input(prompt_text..': ')
  return input
end


------------------------------------ VARIABLES -------------------------------------
local separators = {'_', '-', '*', '#'}
local min_len = 40 -- one side of separator
vim.g.wrap_separator_char = separators[2]
------------------------------------------------------------------------------------


function M.wrap()
  local c_start, c_end = unpack({ getCommentTuple() }) -- comment tuple
  local info = string.upper(prompt('Wrap with')) -- selected info text

  local buf = vim.api.nvim_get_current_buf()

  local _, lnum_start = unpack(vim.fn.getpos("'<"))
  local _, lnum_end = unpack(vim.fn.getpos("'>"))
  local lines_len = lnum_end - lnum_start + 1

  local lines = vim.api.nvim_buf_get_lines(buf, lnum_start - 1, lnum_end, true)

  local sep = string.rep(vim.g.wrap_separator_char, min_len * 2)

  local info_text = ' ' .. info .. ' '
  local info_len = string.len(info_text)

  local sep_half = string.sub(sep, 0, (min_len - info_len / 2))

  local wrapper_start = sep_half .. info_text .. sep_half
  local wrapper_end   = sep

  wrapper_start = c_start ..  wrapper_start  .. c_end
  wrapper_end = c_start .. string.sub(wrapper_end, 0, string.len(wrapper_end) - info_len) .. info_text  .. c_end

  -- info_len

  local wrapper_start_len = string.len(wrapper_start)
  if (wrapper_start_len < string.len(wrapper_end)) then
    wrapper_start = string.sub(wrapper_start, 0, wrapper_start_len - string.len(c_end) - 1) .. vim.g.wrap_separator_char .. string.sub(wrapper_start, wrapper_start_len - string.len(c_end), wrapper_start_len)
  end

  table.insert(lines, 1, wrapper_start)
  table.insert(lines, 1, '')
  table.insert(lines, lines_len + 3, wrapper_end)
  table.insert(lines, lines_len + 4, '')

  vim.api.nvim_buf_set_lines(buf, lnum_start - 1, lnum_start-1 + lines_len, true, lines)
end

return M
