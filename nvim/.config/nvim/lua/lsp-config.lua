local on_attach = function(client, bufnr)
  -- print(client.name)
  client.server_capabilities.document_formatting = true

  local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
  local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

  buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  local opts = { noremap=true, silent=true }

  buf_set_keymap('n', 'gD', '<Cmd>lua vim.lsp.buf.declaration()<CR>', opts)
  buf_set_keymap('n', 'gd', '<Cmd>lua vim.lsp.buf.definition()<CR>', opts)
  buf_set_keymap('n', 'K', '<Cmd>lua vim.lsp.buf.hover()<CR>', opts)
  buf_set_keymap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
  buf_set_keymap('n', '<space>si', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
  -- buf_set_keymap('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
  buf_set_keymap('n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
  buf_set_keymap('n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
  buf_set_keymap('n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
  buf_set_keymap('n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
  buf_set_keymap('n', '<space>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
  buf_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
  buf_set_keymap('n', '<space>e', '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>', opts)
  buf_set_keymap('n', '[d', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)
  buf_set_keymap('n', ']d', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>', opts)
  buf_set_keymap('n', '<space>q', '<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>', opts)

  buf_set_keymap('n', '<space>a', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)

  -- Set some keybinds conditional on server capabilities
  if client.server_capabilities.document_formatting then
    buf_set_keymap("n", "<space>f", "<cmd>lua vim.lsp.buf.format { async = true }<CR>", opts)
    buf_set_keymap("n", "<space>fl", ":checktime<CR><L>", opts)
    -- buf_set_keymap("n", "<space>fl", "<cmd>bufdo e<CR>", opts)
  elseif client.server_capabilities.document_range_formatting then
    buf_set_keymap("n", "<space>f", "<cmd>lua vim.lsp.buf.range_formatting()<CR>", opts)
  end

  -- -- Set autocommands conditional on server_capabilities
  -- if client.server_capabilities.document_highlight then
  --   vim.api.nvim_exec([[
  --   augroup lsp_document_highlight
  --   autocmd! * <buffer>
  --   autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
  --   autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
  --   augroup END
  --   ]], false)
  -- end

  -- if client.name == 'java' then
  -- if client.name == 'jdtls' then
  --   require'jdtls'.setup.add_commands()
  --   buf_set_keymap('n', '<space>a', '<cmd>lua require"jdtls".code_action()<CR>', opts)
  -- end

  if client.name == 'jdtls' then
    require('jdtls').setup_dap({ hotcodereplace = 'auto' })
    require('jdtls.dap').setup_dap_main_class_configs()

    buf_set_keymap('n', '<space>dp', '<cmd>lua require("dap").toggle_breakpoint()<CR>', opts)
    buf_set_keymap('n', '<space>dc', '<cmd>lua require("dap").continue()<CR>', opts)
    buf_set_keymap('n', '<space>do', '<cmd>lua require("dap").step_over()<CR>', opts)
    buf_set_keymap('n', '<space>di', '<cmd>lua require("dap").step_into()<CR>', opts)
  end

end

-- Configure lua language server for neovim development
local lua_settings = {
Lua = {
  runtime = {
    -- LuaJIT in the case of Neovim
    version = 'LuaJIT',
    path = vim.split(package.path, ';'),
  },
  diagnostics = {
    -- Get the language server to recognize the `vim` global
    globals = {'vim'},
  },
  workspace = {
    -- Make the server aware of Neovim runtime files
    library = {
      [vim.fn.expand('$VIMRUNTIME/lua')] = true,
        [vim.fn.expand('$VIMRUNTIME/lua/vim/lsp')] = true,
      },
    },
  }
}

-- local capabilities = vim.lsp.protocol.make_client_capabilities()
local capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())
capabilities.textDocument.completion.completionItem.snippetSupport = true

local function make_config()
  return {
    capabilities = capabilities,
    on_attach = on_attach,
  }
end


local lsp_installer = require("nvim-lsp-installer")

lsp_installer.on_server_ready(function(server)
    local config = make_config()

    if server == "lua" then
      config.settings = lua_settings
    end

    if server.name == "jdtls" then
      config.use_lombok_agent = true

      config.flags = {
        allow_incremental_sync = true,
      }
      config.filetypes = { "java" }
      config.capabilities.workspace = {
        configuration = true
      }
      config.capabilities.textDocument = {
        completion = {
          completionItem = {
            snippetSupport = true
          }
        }
      }
      config.settings = {
        java = {
          configuration = {
            runtimes = {
              {
                name = "JavaSE-11",
                path = "~/dev/java/openjdk-11.0.13_8",
              },
              {
                name = "JavaSE-17",
                path = "~/dev/java/jdk-17",
              },
            }
          },
          codeGeneration = {
            toString = {
              template = "${object.className}{${member.name()}=${member.value}, ${otherMembers}}",
              -- codeStyle = "STRINGS_BUILDER_CHAINED",
              codeStyle = "STRING_CONCATENATION",
              listArrayContents = true,
              useBlocks = true,
            },
          },
          sources = {
            organizeImports = {
              starThreshold = 9999;
              staticStarThreshold = 9999;
            }
          },
          contentProvider = { preferred = 'fernflower' };
          implementationsCodeLens = { enabled = true },
          referencesCodeLens = { enabled = true },
          signatureHelp = { enabled = true },
          completion = {
            favoriteStaticMembers = {
              "org.hamcrest.MatcherAssert.assertThat",
              "org.hamcrest.Matchers.*",
              "org.hamcrest.CoreMatchers.*",
              "org.junit.jupiter.api.Assertions.*",
              "java.util.Objects.requireNonNull",
              "java.util.Objects.requireNonNullElse",
              "org.mockito.Mockito.*"
            }
          },
        },
      }

      

      local extendedClientCapabilities = require'jdtls'.extendedClientCapabilities
      extendedClientCapabilities.resolveAdditionalTextEditsSupport = true

      -- print(os.getenv "HOME")
      local bundles = {}
      vim.list_extend(bundles, vim.split(vim.fn.glob("/home/czr/.config/nvim/java_dep/com.microsoft.java.debug.plugin-*.jar"), "\n"))
      vim.list_extend(bundles, vim.split(vim.fn.glob("/home/czr/.config/nvim/java_dep/server/*.jar"), "\n"))

      config.init_options = {
        extendedClientCapabilities = extendedClientCapabilities,
        bundles = bundles,
      }


      -- print(os.getenv'JDTLS_JVM_ARGS')


      -- export JDTLS_JVM_ARGS="-javaagent:$HOME/.config/nvim/java_dep/lombok-1.18.24.jar"

      -- print(vim.inspect(config))
      -- print(vim.inspect(require'jdtls'))

--       local finders = require'telescope.finders'
--       local sorters = require'telescope.sorters'
--       local actions = require'telescope.actions'
--       local pickers = require'telescope.pickers'
-- 
--       require('jdtls.ui').pick_one_async = function(items, prompt, label_fn, cb)
--         local opts = {}
--         pickers.new(opts, {
--           prompt_title = prompt,
--           finder    = finders.new_table {
--             results = items,
--             entry_maker = function(entry)
--               return {
--                 value = entry,
--                 display = label_fn(entry),
--                 ordinal = label_fn(entry),
--               }
--             end,
--           },
--           sorter = sorters.get_generic_fuzzy_sorter(),
--           attach_mappings = function(prompt_bufnr)
--             actions.goto_file_selection_edit:replace(function()
--               local selection = actions.get_selected_entry(prompt_bufnr)
--               actions.close(prompt_bufnr)
-- 
--               cb(selection.value)
--             end)
-- 
--             return true
--           end,
--         }):find()
--       end

      -- Server

      -- require('jdtls').start_or_attach(config)

    end

    -- if server == "efm" then
    --   config.cmd = { vim.fn.stdpath("data") .. "/lsp_servers/efm/efm-langserver" }
    --   config.flags = { debounce_text_changes = 150 }
    --   config.filetypes = {"javascript", "html", "css", "typescript"}
    --   config.init_options = {documentFormatting = true}
    -- end

    if server == "eslint" then
      config.cmd = {vim.fn.stdpath("data") .. "/lsp_servers/vscode_eslint/node_modules/.bin/vscode-eslint-language-server --stdio"}
      config.settings = {
        format = { enable = true }, -- this will enable formatting
      }
    end

    -- if server == "emmet_ls" then
    --   print('emmet online')
    --   config.cmd = {vim.fn.stdpath("data") .. "/lsp_servers/emmet_ls/node_modules/.bin/emmet-ls --stdio"}
    --   config.filetypes = {'html', 'css'}
    -- end

  --     if server == "angularls" then
  --       local function get_probe_dir(root_dir)
  --         local project_root = util.find_node_modules_ancestor(root_dir)
  -- 
  --         return project_root and (project_root .. '/node_modules') or ''
  --       end
  -- 
  --       local default_probe_dir = get_probe_dir(vim.fn.getcwd())
  -- 
  --       config.cmd = {
  --         vim.fn.stdpath("data") .. "/lsp_servers/angularls/node_modules/.bin/ngserver", '--stdio',
  --         '--tsProbeLocations', default_probe_dir, '--ngProbeLocations', default_probe_dir,
  --       }
  --     end

    server:setup(config)
    vim.cmd [[ do User LspAttachBuffers ]]
end)

