vim.g.mapleader = ' '  -- 'vim.g' sets global variables

-- vim.api.nvim_set_keymap('i', '{<CR>', '{<CR>}<ESC>O', {noremap = true, silent = true})


vim.api.nvim_set_keymap('n', '<leader>/', ':CommentToggle<CR>', {noremap = true, silent = true})

-- better indenting
vim.api.nvim_set_keymap('v', '<', '<gv', {noremap = true, silent = true})
vim.api.nvim_set_keymap('v', '>', '>gv', {noremap = true, silent = true})

-- leave insert mode
-- vim.api.nvim_set_keymap('i', 'jk', '<ESC>', {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('i', 'kj', '<ESC>', {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('i', 'jj', '<ESC>', {noremap = true, silent = true})

-- faster navigation
vim.api.nvim_set_keymap('v', '<C-j>', '5j', {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', '<C-j>', '5j', {noremap = true, silent = true})
vim.api.nvim_set_keymap('v', '<C-k>', '5k', {noremap = false, silent = true})
vim.api.nvim_set_keymap('n', '<C-k>', '5k', {noremap = true, silent = true})

-- tabs
-- vim.api.nvim_set_keymap('n', '<C-A-j>', ':tabprevious<CR>', {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', '<C-A-k>', ':tabnext<CR>', {noremap = true, silent = true})

-- vim.api.nvim_set_keymap('n', '<A-n>', ':tabnew<CR>', {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', '<A-k>', ':tabnext<CR>', {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', '<A-j>', ':tabprevious<CR>', {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', '<A-c>', ':tabclose<CR>', {noremap = true, silent = true})

-- splits
vim.api.nvim_set_keymap('n', '<C-s>', ':split<CR>', {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', '<A-s>', ':vsp<CR>', {noremap = true, silent = true})

-- clear buffer
-- vim.api.nvim_set_keymap('n', '<leader>bd', ':%bd <bar> e#', {noremap = false, silent = false})

vim.api.nvim_set_keymap('n', '<C-c>', '"+y', {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', '<leader>l', ':noh<CR>', {noremap = true, silent = true})

vim.api.nvim_set_keymap('n', 'Y', 'y$', {noremap = true, silent = true})       -- yanks to the end of line
vim.api.nvim_set_keymap('n', 'n', 'nzzzv', {noremap = true, silent = true})    -- centers cursors
vim.api.nvim_set_keymap('n', 'N', 'Nzzzv', {noremap = true, silent = true})    -- centers cursors
vim.api.nvim_set_keymap('n', 'J', 'mzJ`z', {noremap = true, silent = true})    -- centers cursors
vim.api.nvim_set_keymap('i', ',', ',<C-g>u', {noremap = false, silent = true}) -- inserts undo point when I press ,

vim.cmd [[
  nnoremap <expr> k (v:count > 5 ? "m'" . v:count : "") . 'k'
  nnoremap <expr> j (v:count > 5 ? "m'" . v:count : "") . 'j'

  vnoremap <C-A-j> :m '>+1<CR>gv=gv
  vnoremap <C-A-k> :m '<-2<CR>gv=gv

  inoremap <C-A-j> <esc>:m .+1<CR>==
  inoremap <C-A-k> <esc>:m .-2<CR>==

  nnoremap <C-A-j> :m .+1<CR>==
  nnoremap <C-A-k> :m .-2<CR>==
]]

vim.api.nvim_set_keymap('n', '0', '^', {noremap = true, silent = true})
vim.api.nvim_set_keymap('v', '<leader>p', '_dp', {noremap = true, silent = true})
vim.api.nvim_set_keymap('v', '<leader>P', '_dP', {noremap = true, silent = true})


-- UTILITY FUNCTIONS --
vim.api.nvim_set_keymap('n', '<leader>j', ': lua Joke()<CR>', {noremap = true, silent = true})
vim.api.nvim_set_keymap('v', '<leader>c', ': lua require("wrap-comment-info").wrap()<CR>', {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', '<leader>m', ': lua require("treesitter-testing").test()<CR>', {noremap = true, silent = true})


vim.keymap.set("n", '<leader>s', ': %s/\\<<C-r><C-w>\\>/<C-r><C-w>/gI<Left><Left><Left>', {noremap = false})
