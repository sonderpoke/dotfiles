require('indent_guides').setup({
  indent_levels = 30;
  indent_guide_size = 1;
  indent_start_level = 1;
  indent_enable = true;
  indent_space_guides = true;
  indent_tab_guides = false;
  indent_soft_pattern = '\\s';
  exclude_filetypes = {'help','dashboard','dashpreview','NvimTree','vista','sagahover'};

  even_colors = { fg ='none',bg='none' };
  odd_colors = {fg='none',bg='none'};
  -- even_colors = { fg ='#2a3834',bg='#332b36' };
  -- odd_colors = {fg='#332b36',bg='#2a3834'};

  -- even_colors = { fg ='#40434c',bg='#3b404c' };
  -- odd_colors = {fg='#3b404c',bg='#40434c'};
  -- even_colors = { fg ='#2b2f38',bg='#2b2f38' };
  -- odd_colors = {fg='#2b2f38',bg='#2b2f38'};
})

