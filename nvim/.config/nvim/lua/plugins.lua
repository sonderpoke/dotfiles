-- vim.cmd [[packadd packer.nvim]]

-- return require('packer').startup(function()
--   -- Packer can manage itself
--   use 'wbthomason/packer.nvim'
--   use {'terrortylor/nvim-comment', opt = true}
-- end)

local execute = vim.api.nvim_command
local fn = vim.fn

local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"

if fn.empty(fn.glob(install_path)) > 0 then
  execute("!git clone https://github.com/wbthomason/packer.nvim " .. install_path)
  execute "packadd packer.nvim"
end

--- Check if a file or directory exists in this path
local function require_plugin(plugin)
local plugin_prefix = fn.stdpath("data") .. "/site/pack/packer/opt/"

local plugin_path = plugin_prefix .. plugin .. "/"
local ok, err, code = os.rename(plugin_path, plugin_path)
if not ok then
  if code == 13 then
    -- Permission denied, but it exists
    return true
  end
end
--	print(ok, err, code)
if ok then
  vim.cmd("packadd " .. plugin)
end
return ok, err, code
end

vim.cmd "autocmd BufWritePost plugins.lua PackerCompile" -- Auto compile when there are changes in plugins.lua

return require("packer").startup(
function(use)
  use "wbthomason/packer.nvim" -- Packer can manage itself as an optional plugin

-------------------------------------------------------------------

--LSP--
  -- use {"neovim/nvim-lspconfig", opt = true}
  -- use {"kabouzeid/nvim-lspinstall", opt = true}
  use {
    'neovim/nvim-lspconfig',
    'williamboman/nvim-lsp-installer',
  }
  use {"hrsh7th/cmp-nvim-lsp", opt = true}
  use {"hrsh7th/cmp-buffer", opt = true}
  -- use {"hrsh7th/vim-vsnip", opt = true}
  -- use {"hrsh7th/cmp-vsnip", opt = true}

  -- use {"rafamadriz/friendly-snippets"}
  use {"hrsh7th/nvim-cmp"}
  use {"editorconfig/editorconfig-vim"}

  use 'L3MON4D3/LuaSnip'
  use 'rafamadriz/friendly-snippets'
  use 'saadparwaiz1/cmp_luasnip'
  -- use {
  --   'L3MON4D3/LuaSnip',
  --   -- after = "nvim-cmp",
  --   requires = {{"rafamadriz/friendly-snippets"}},
  --   config = function()
  --     require("luasnip/loaders/from_vscode").lazy_load{ paths = {}}
  --     require("luasnip/loaders/from_vscode").lazy_load()
  --   end
  --   -- config = function() require'completion'.luasnip() end
  -- }
  -- use {
  --   "saadparwaiz1/cmp_luasnip",
  --   after = "LuaSnip",
  -- }

  -- use {"ThePrimeagen/harpoon"}
  use {"nvim-lua/popup.nvim"}
  use {"nvim-lua/plenary.nvim"}

  use {"ggandor/lightspeed.nvim"}

  use {"kkoomen/vim-doge"}

  use {'neoclide/coc.nvim', opt = true}

  use {'mfussenegger/nvim-jdtls'}

--------------------------------------- DAP ----------------------------------------
  use {'mfussenegger/nvim-dap'}
  use {
    'rcarriga/nvim-dap-ui',
    requires = 'mfussenegger/nvim-dap'
  }
  use 'theHamsta/nvim-dap-virtual-text'
------------------------------------------------------------------------------------


  use {"onsails/lspkind-nvim", opt = true}
--LSP--

--GIT--
  use {'tpope/vim-fugitive', opt = true}
  use {'airblade/vim-gitgutter', opt = true}
--GIT--

  use {'nvim-treesitter/nvim-treesitter', run=':TSUpdate'}
  use {'nvim-treesitter/playground'}
  use {
    'nvim-telescope/telescope.nvim',
    requires = {{'nvim-lua/popup.nvim'}, {'nvim-lua/plenary.nvim'}}
  }

  -- use { "nvim-telescope/telescope-file-browser.nvim" }

--UTILS--


  --------------------------------------- REST ---------------------------------------
  -- use { "NTBBloodbath/rest.nvim",
  --   requires = { "nvim-lua/plenary.nvim" }
  -- }
  ---------------------------------------------------------------------------- REST --


  use {
    'kyazdani42/nvim-tree.lua',
    requires = {
      'kyazdani42/nvim-web-devicons'
    },
  }

  use {
    'nvim-lualine/lualine.nvim',
    requires = { 'kyazdani42/nvim-web-devicons' }
  }

  use {"terrortylor/nvim-comment", opt = true}
  use {'tpope/vim-surround', opt = true}
  use {'romgrk/barbar.nvim', opt = true}
  use {'vimwiki/vimwiki', opt = true}
  use {'mhartington/formatter.nvim', opt = true}

  use {'glepnir/indent-guides.nvim', opt = true}
  use {"lukas-reineke/indent-blankline.nvim"}
  use {'windwp/nvim-autopairs', opt = true}
  use {'windwp/nvim-ts-autotag', opt = true}
--UTILS--

  -- use {"kevinhwang91/nvim-bqf", opt = true}

--THEMES--
  use {'npxbr/gruvbox.nvim', requires = {"rktjmp/lush.nvim"}}
  use {'shaunsingh/moonlight.nvim', opt = true}
  use {'folke/tokyonight.nvim', opt = true}
  use {'monsonjeremy/onedark.nvim', opt = true}
--THEMES--
-------------------------------------------------------------------

--LSP--
  -- require_plugin("coc.nvim")
  -- require_plugin("nvim-lspconfig")
  require_plugin("cmp-buffer")
  require_plugin("cmp-nvim-lsp")
  -- require_plugin("cmp-vsnip")

  require_plugin("lspkind-nvim")
--   require_plugin("nvim-dap")
--LSP--

--GIT--
  require_plugin("vim-fugitive")
  require_plugin("vim-gitgutter")
--GIT--

--UTILS--
  require_plugin("nvim-comment")
  require_plugin("vim-surround")
  require_plugin("barbar.nvim")
  require_plugin("vimwiki")
  -- require_plugin("LuaSnip")
  -- require_plugin("vim-vsnip")
  -- require_plugin("friendly-snippets")
  require_plugin("mhartington/formatter.nvim")
  -- require_plugin("indent-guides.nvim")
  require_plugin("nvim-autopairs")
  require_plugin("nvim-ts-autotag")
--UTILS--

  require_plugin("nvim-treesitter")
  -- require_plugin("nvim-tree.lua")
  -- require_plugin("nvim-bqf")

--THEMES--
  require_plugin("tokyonight.nvim")
  require_plugin("moonlight.nvim")
  require_plugin("onedark.nvim")
--THEMES--

end
)
