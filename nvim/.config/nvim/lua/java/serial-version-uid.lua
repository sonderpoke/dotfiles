local M = {}

function M.generateSerialUID()
  local extension = vim.fn.expand('%:e')
  if extension == 'java' then
    local cwd = vim.fn.getcwd()
    local name = vim.fn.expand('%:t')

    local dir = vim.fn.expand('%:p:h')
    local filedir = dir..'/'..name
    local filename = name:gsub(".java", "")

    local packageLine = vim.fn.readfile(filedir)[1]
    local package = string.sub(packageLine, 9, string.len(packageLine)-1)
    local class = package.."."..filename

    local command = "serialver -classpath "..cwd.."/target/classes "..class

    local result = ''
    local handle = io.popen(command)
    result = handle:read("*a"):gsub(class..":", "")
    result = result:gsub("\r", "")
    result = result:gsub("\n", "")
    handle:close()

    local row, col = unpack(vim.api.nvim_win_get_cursor(0))
    local buf = vim.api.nvim_get_current_buf()
    local lines = {result}

    print(lines[1])

    vim.api.nvim_buf_set_lines(buf, row, row, true, lines)
    return result
  end
end

return M
