local M = {}

local nt_api = require("nvim-tree.api")
local api = vim.api


--------------------------------- RENAME FUNCTION ----------------------------------
local function doRename(buf, rootPaths, selectedLines, selectedLines_len)
  local changedLineCount = api.nvim_buf_line_count(buf)

  if (changedLineCount ~= selectedLines_len) then
    print("Line count doesn't match, if you don't want to rename, leave that line unchanged")
    return
  end

  local changedLines = api.nvim_buf_get_lines(buf, 0, changedLineCount, true)

  local cmds = {}

  for idx, changedLine in pairs(changedLines) do
    if (changedLine ~= selectedLines[idx]) then
      local root_path = rootPaths[idx]
      local subtracted_path = string.sub(root_path, 0, string.len(root_path) - string.len(selectedLines[idx]))

      local createDirCmd = ""
      local slash_idx = string.find(string.reverse(changedLine), '/', 1, true)

      if (slash_idx ~= nil and slash_idx > 0) then
        createDirCmd = string.format("mkdir -p %s%s && ",
            subtracted_path,
            string.sub(changedLine, 1, string.len(changedLine) - slash_idx))
      end

      local cmd = string.format("%s mv '%s' '%s%s' ", createDirCmd, root_path, subtracted_path, changedLine)

      local count = 0
      for i = 1, selectedLines_len, 1 do
        if (string.find(rootPaths[i], root_path)) then
          count = count + 1
        end
      end

      if (count < 2) then -- files should be changed first
        table.insert(cmds, 1, cmd)
      else
        table.insert(cmds, cmd)
      end
    end
  end

  local final_cmd = table.concat(cmds, '&& ')
  print(final_cmd)

  if (string.len(final_cmd) > 0) then
    local handle = io.popen(final_cmd)
    handle:read("*a") -- needed to reload tree after cmd applied
    nt_api.tree.reload()
    handle.close()
  end

end
----------------------------------------------------------------- RENAME FUNCTION --


------------------------------- REMOVE AUTO COMMAND --------------------------------
local function removeAutoCommand(buf)
  for _, aucmd in pairs(api.nvim_get_autocmds({event = "WinClosed"})) do
    if (aucmd['buffer'] == buf) then
      api.nvim_del_autocmd(aucmd['id'])
    end
  end
end
------------------------------------------------------------- REMOVE AUTO COMMAND --


---------------------------------- CREATE WINDOW -----------------------------------
local function createWindow(buf, content, selectedLines_len)
  local content_copy = content
  api.nvim_buf_set_lines(buf, 0, -1, true, content_copy)

  local height = vim.api.nvim_list_uis()[1].height / 3

  local opts = {
    relative = 'editor',
    width = 80,
    -- height = 30
    height = selectedLines_len + 5,
    col =  1,
    row = height,
    anchor = 'NW',
    style = 'minimal'
  }

  return api.nvim_open_win(buf, 1, opts)
end
------------------------------------------------------------------- CREATE WINDOW --


-------------------------------- GET SELECTED LINES --------------------------------
local function getSelectedLines()
  local _, lnum_start = unpack(vim.fn.getpos("'<"))
  local _, lnum_end = unpack(vim.fn.getpos("'>"))

  local formatedLines_len = lnum_end - lnum_start + 1

  local lines = api.nvim_buf_get_lines(
    api.nvim_get_current_buf(),
    lnum_start - 1,
    lnum_end,
    true
  )

  local selectedLines = {}

  for _, v in pairs(lines) do
    local str = string.find(v, "%p?%w")
    table.insert(selectedLines, string.sub(v, str, string.len(v)))
  end

  return {selectedLines, formatedLines_len}
end
-------------------------------------------------------------- GET SELECTED LINES --


------------------------------- PROCESS CHILD NODES --------------------------------
local function processChildNodes(nodeLines, nodes, alreadyInserted, selectedLines, selectedLines_len)
  for _, child_node in pairs(nodes) do
    if (alreadyInserted[1] > selectedLines_len) then return end

    if (selectedLines[alreadyInserted[1] + 1] == child_node['name']) then
      table.insert(nodeLines, child_node['absolute_path'])
      alreadyInserted[1] = alreadyInserted[1] + 1
      if (alreadyInserted[1] == selectedLines_len) then return end
    end

    if (child_node['open']) then
      processChildNodes(nodeLines, child_node['nodes'], alreadyInserted, selectedLines, selectedLines_len)
      if (alreadyInserted[1] == selectedLines_len) then return end
    end
  end
end
------------------------------------------------------------- PROCESS CHILD NODES --


---------------------------------- GET ROOT PATHS ----------------------------------
local function getRootPaths(selectedLines, selectedLines_len)
  local node = nt_api.tree.get_node_under_cursor()
  local nodeLines = {}
  local alreadyInserted = {0}
  processChildNodes(nodeLines, node['parent']['nodes'], alreadyInserted, selectedLines, selectedLines_len)
  return nodeLines
end
------------------------------------------------------------------ GET ROOT PATHS --


---------------------------------- MAIN FUNCTION  ----------------------------------
function M.rename()
  local selectedLines, selectedLines_len = unpack(getSelectedLines())
  local rootPaths = getRootPaths(selectedLines, selectedLines_len)
  local buf = api.nvim_create_buf(false, true)

  createWindow(buf, selectedLines, selectedLines_len)

  api.nvim_create_autocmd(
    {"WinClosed"},
    {
      buffer = buf,
      callback = function()
        doRename(buf, rootPaths, selectedLines, selectedLines_len)
        removeAutoCommand(buf)
      end
    }
  )
end
------------------------------------------------------------------ MAIN FUNCTION  --

return M

