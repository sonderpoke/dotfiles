vim.api.nvim_set_keymap('n', '<leader>h', ':lua require("harpoon.ui").toggle_quick_menu()<CR>', {noremap = true, silent = true})

vim.api.nvim_set_keymap('n', '<C-a-g>', ':lua require("harpoon.mark").add_file()<CR>', {noremap = true, silent = true})

vim.api.nvim_set_keymap('n', '<C-a-a>', ':lua require("harpoon.ui").nav_file(1)<CR>', {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', '<C-a-s>', ':lua require("harpoon.ui").nav_file(2)<CR>', {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', '<C-a-d>', ':lua require("harpoon.ui").nav_file(3)<CR>', {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', '<C-a-f>', ':lua require("harpoon.ui").nav_file(4)<CR>', {noremap = true, silent = true})

