vim.cmd('source ~/dotfiles/nvim/coc.vim')

-- Coc
-- vim.api.nvim_set_keymap('n', '<leader>rn', '<Plug>(coc-rename)', {silent = true})
-- vim.api.nvim_set_keymap('n', '<leader>rn', ':CocCommand tslint.fixAllProblems <CR>', {silent = true})
-- 
-- vim.api.nvim_set_keymap('n', '<leader>rw', ':CocSearch <C-R>=expand("<cword>")<CR><CR>', {silent = true})
-- vim.api.nvim_set_keymap('n', '<leader>fw', ':CocSearch -S ', {noremap = true})
-- vim.api.nvim_set_keymap('n', '<leader>a', ':CocAction<CR>', {silent = true})
-- 
-- vim.api.nvim_set_keymap('', '<C-n>', ':CocCommand explorer<CR>', {silent = true})

