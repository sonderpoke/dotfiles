local cmd = vim.cmd
local o = vim.o
local bo = vim.bo
local wo = vim.wo

cmd('syntax on') -- syntax highlighting
-- cmd('set iskeyword-=_') -- treat dash separated words as a word text object"
-- cmd('set listchars+=space:.')
cmd('set clipboard+=unnamedplus') -- Copy paste between vim and everything else
cmd('set list')
cmd [[ filetype plugin indent on ]] -- indentation based on filetype
cmd('set colorcolumn=140')

-- cmd('e ++ff=unix') // delete windows spaces

-- o.foldmethod="indent"
-- o.foldlevelstart=20
-- o.foldlevel=20
-- o.foldnestmax=20
-- cmd [[ autocmd Syntax java,ts,html setlocal foldmethod=syntax ]]
-- cmd [[ autocmd Syntax java,ts,html normal zR ]]


wo.spell = false

bo.sw=2
bo.ts=2
bo.smartindent = true -- Makes indenting smart
bo.expandtab = true
bo.autoindent = true
bo.swapfile = false
bo.grepprg = 'rg'

o.encoding = "utf-8"
o.fileencoding = "utf-8" -- The encoding written to file
o.scrolloff = 5
o.wrap=false
o.cmdheight = 2 -- More space for displaying messages

--o.t_Co = "256" -- Support 256 colors
o.title = true

wo.number = true -- set numbered lines
wo.relativenumber = true -- set relative number
wo.cursorline = true -- Enable highlighting of the current line
wo.signcolumn = "yes" -- Always show the signcolumn, otherwise it would shift the text each time

o.showtabline = 2 -- Always show tabs

o.splitright = false -- Vertical splits will automatically be to the right
o.splitbelow = false -- Horizontal splits will automatically be below

o.termguicolors = true -- set term gui colors most terminals support this
o.mouse = "a" -- Enable your mouse
o.mousefocus = true

o.conceallevel = 0 -- So that I can see `` in markdown files
o.backup = false -- This is recommended by coc
o.writebackup = false -- This is recommended by coc

o.updatetime = 300 -- Faster completion
o.timeoutlen = 300 -- By default timeoutlen is 1000 ms

--o.colorcolumn = 140

-- o.guifont = "Fira Code:h17"

o.laststatus = 3
