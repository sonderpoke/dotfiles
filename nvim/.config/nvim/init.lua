CONFIG_PATH = vim.fn.stdpath('config')
vim.cmd('source'..CONFIG_PATH..'/functions.vim')
-- require('l-coc')

require('git-message')
require('bindings')
require('settings')
require('plugins')
require('nvim_comment').setup()
require('l-treesitter')
require('l-telescope')
-- require('l-telescope-file-browser')
require('l-lualine')
require('l-harpoon')
require('l-barbar')
-- require('l-rest')
require('nvim-autopairs').setup()

-- require('l-indent-lines')

local g = vim.g
g.indentLine_enabled = 1
g.indent_blankline_char = "▏"

g.indent_blankline_filetype_exclude = {"help", "terminal", "dashboard"}
g.indent_blankline_buftype_exclude = {"terminal"}

g.indent_blankline_show_trailing_blankline_indent = false
g.indent_blankline_show_first_indent_level = false

--THEMES
vim.o.background = "dark" -- or "light" for light mode
vim.cmd([[colorscheme gruvbox]])

-- require('moonlight').set()

-- vim.g.tokyonight_style = "night"
-- vim.cmd[[colorscheme tokyonight]]

-- vim.cmd[[colorscheme onedark]] --bg color 282c34
-- vim.cmd[[hi Whitespace guifg=#282c34]]
-- vim.cmd[[hi Whitespace guifg=#2b2f38]]
-- vim.cmd[[hi Normal guibg=#1e2127]]


vim.cmd[[hi SignColumn guibg=none]]
vim.cmd[[hi CursorLineNR guibg=None]]
vim.cmd[[hi LineNr guifg=#5eacd3]]
-- vim.cmd[[hi Normal guibg=#0D0D0D]]


-- vim.cmd[[hi Whitespace guifg=#212121]]

-- vim.cmd[[hi Comment guifg=#0c0c0c]]
--THEMES


-- Coc
-- require('l-coc.bindings')

require('lsp.compe-ls')
require('lsp.lua-ls')
require('lsp-config')
require('lsp.typescript-ls')
require('lsp')

-- require('lsp.java-ls').setup()
require('l-nvimtree')

--------------------------------------- DAP ----------------------------------------
require('lsp.nvim-dap').setup()
require('lsp.nvim-dap-ui').setup()
require('nvim-dap-virtual-text').setup()
------------------------------------------------------------------------------------


-- require('snippets')
-- require('lsp.java-ls')

-- require('lsp.completion-ls')
-- require('lsp.angular-ls')
-- require('lsp.emmet-ls')
-- require('lsp.html-ls')
-- require('lsp.java-ls')
-- require('lsp.json-ls')


-- require('lsp.bash-ls')
-- require('lsp.clangd')
-- require('lsp.css-ls')
-- require('lsp.dart-ls')
-- require('lsp.docker-ls')
-- require('lsp.efm-general-ls')
-- require('lsp.elm-ls')
-- require('lsp.graphql-ls')
-- require('lsp.go-ls')
-- require('lsp.js-ts-ls')
-- require('lsp.kotlin-ls')
-- require('lsp.latex-ls')
-- require('lsp.lua-ls')
-- require('lsp.php-ls')
-- require('lsp.python-ls')
-- require('lsp.ruby-ls')
-- require('lsp.rust-ls')
-- require('lsp.svelte-ls')
-- require('lsp.terraform-ls')
-- require('lsp.tailwindcss-ls')
-- require('lsp.vim-ls')
-- require('lsp.vue-ls')
-- require('lsp.yaml-ls')


function Joke()
  local command = "curl -s https://v2.jokeapi.dev/joke/Programming\\?type\\=single | jq '.joke'"
  local handle = io.popen(command)
  local result = handle:read("*a")
    print(result)
  handle:close()
end

