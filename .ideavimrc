" Plug 'preservim/nerdtree'

set clipboard+=unnamed
" resolves import with copy/paste within ide
set clipboard+=ideaput
set idearefactormode=keep
set ignorecase smartcase
set hlsearch
set showmode
set scrolloff=5
set history=1000
set surround
set incsearch
set relativenumber
set number
set smartcase
set hlsearch
set visualbell
"set noerrorbells
set easymotion
set commentary
set ideajoin
set exchange
set multiple-cursors
set sneak
set matchit
set NERDTree
set which-key

" Tab/Window navigation
"nnoremap <Tab> <C-w><C-w>
map <S-Tab> <Action>(MoveEditorToOppositeTabGroup)
map <C-\> <Action>(SplitVertically)

"nnoremap H gT
"nnoremap L gt
nnoremap <A-j> gT
nnoremap <A-k> gt
"nnoremap <A-c> :action CloseActiveTab
nnoremap <A-c> :action CloseContent<CR>

let mapleader = " "
"map <Leader>w :w<CR>
"map <Leader>wq :wq<CR>
map <Leader>q :q<CR>
"map <Leader>t :NERDTree<CR>
map <C-n> :NERDTree<CR>

" IntelliJ action mappings
map \r <Action>(ReformatCode)
map \e <Action>(RecentFiles)
map [[ <Action>(MethodUp)
map ]] <Action>(MethodDown)
map gi <Action>(GotoImplementation)
map gd <Action>(GotoDeclaration)
map gp <Action>(GotToSuperMethod)
map gt <Action>(GotoTest)
map g] <Action>(ShowUsages)
"map c[ <Action>(VcsShowPrevChangeMarker)
"map c] <Action>(VcsShowNextChangeMarker)
map <S-r> <Action>(RunClass)
map <C-o> <Action>(Back)
map <C-i> <Action>(Forward)
"map <C-w> <Action>(CloseContent)
"map <C-w> <Action>(WindowPrevAction)
map <S-Space> <Action>(GotoNextError)
map <Leader>d <Action>(Debug)
map <Leader>r <Action>(RenameElement)
map <Leader>c <Action>(Stop)
map <Leader>z <Action>(ToggleDistractionFreeMode)
"map <Leader>s <Action>(SelectInProjectView)
map <Leader>h <Action>(Vcs.ShowTabbedFileHistory)
map <Leader>b <Action>(ToggleLineBreakpoint)
map <Leader>o <Action>(FileStructurePopup)

" Code folding
map zc <Action>(CollapseRegion)
map zo <Action>(ExpandRegion)
map <Leader>zc <Action>(CollapseAllRegions)
map <Leader>zo <Action>(ExpandAllRegions)

nnoremap Y y$
vnoremap <leader>p "_dP

map <Leader>so :source ~/.ideavimrc
nmap <C-j> 5j
nmap <C-k> 5k
vmap <C-j> 5j
vmap <C-k> 5k

""" Mappings --------------------------------
map <leader>f <Plug>(easymotion-s)
map <leader>e <Plug>(easymotion-f)

map <leader>d <Action>(Debug)
map <leader>r <Action>(RenameElement)
map <leader>c <Action>(Stop)
map <leader>z <Action>(ToggleDistractionFreeMode)

map <leader>s <Action>(SelectInProjectView)
map <leader>a <Action>(Annotate)
map <leader>h <Action>(Vcs.ShowTabbedFileHistory)
map <S-Space> <Action>(GotoNextError)

map <leader>b <Action>(ToggleLineBreakpoint)
map <leader>o <Action>(FileStructurePopup)

map <C-a>j <Action>(Terminal.OpenInTerminal)

nnoremap <Leader>l :nohlsearch<cr>


nmap <Leader>ff <Action>(GotoFile)
nmap <Leader>fg <Action>(FindInPath)
nmap <Leader>gr <Action>(FindUsages)


map <C-A-j> <Action>(MoveLineDown)
map <C-A-k> <Action>(MoveLineUp)

vmap < <gv
vmap > >gv

nnoremap [[ <Action>(MethodUp)
nnoremap ]] <Action>(MethodDown)

inoremap <C-j> <Action>(PopupMenu-selectNext)
inoremap <C-k> <Action>(PopupMenu-selectPrev)


" Window splits
"map <leader>wv <Action>(SplitVertically)
"map <leader>ws <Action>(SplitHorizontally)
"map <leader>wu <Action>(Unsplit)
"map <leader>wm <Action>(MoveEditorToOppositeTabGroup)

" Display options
map <leader>dd <action>(ToggleDistractionFreeMode)
map <leader>dz <action>(ToggleZenMode)
map <leader>df <action>(ToggleFullScreen)

" Actions
map <leader>am <action>(ShowIntentionActions)
map <leader>as <action>(SearchEverywhere)

map <leader><leader> <Action>(RecentFiles)
map <leader>fl <action>(RecentLocations)
map <leader>fs <action>(NewScratchFile)

nmap <leader>gu <Action>(ShowUsages)
nmap <leader>gf <Action>(Back)
nmap <leader>gb <Action>(Forward)
" Git windows
map <leader>gc <Action>(CheckinProject)
map <leader>gs <Action>(ActivateVersionControlToolWindow)
map <leader>gb <Action>(Git.Branches)
map <Leader>ga <Action>(Annotate)

" map <A-S-<> <S-C-PageUp>

nmap 0 ^
vmap 0 ^

nmap <leader>ss yiw:%s/\<<C-R>"\>/<C-R>"/gI<Left><Left><Left>

" paste from clipboard
imap <C-S-V> <ESC>"+pa

nmap <leader> a <A-CR>

" Popup navigation
"inoremap <C-j> <Action>(PopupMenu-selectNext)
"inoremap <C-k> <Action>(PopupMenu-selectPrev)

"command! Reformat action ReformatCode

nmap <leader>wa :wa<CR>
map <leader>wm <Action>(MoveEditorToOppositeTabGroup)

"nnoremap <A-l> <C-w>l
"nnoremap <A-k> <C-w>k

nmap <C-w>w  <C-w>w
nmap <C-w>l  <C-w>l
nmap <C-w>k  <C-w>k

imap <A-h> <ESC>ha
imap <A-j> <ESC>ja
imap <A-k> <ESC>ka
imap <A-l> <ESC>la

nmap q: :<up>
