syntax on
set nospell
set guicursor=
"set nohlsearch
set hidden
set smartcase
set noswapfile
set termguicolors
set scrolloff=5
set mouse=a
set expandtab
set clipboard+=unnamedplus
set number! relativenumber!

set cursorline
set listchars+=space:.
set list
set shiftwidth=2
set autoindent
set smartindent
set textwidth=80
set nowrap

autocmd Filetype java setlocal ts=4 sw=4 expandtab textwidth=80

"----PLUGIN MANAGEMENT----
call plug#begin('~/.vim/plugged')
" Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
" Plug 'junegunn/fzf.vim'
Plug 'vimwiki/vimwiki'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-fugitive'
Plug 'mattn/emmet-vim'
Plug 'mhinz/vim-startify'


Plug 'neovim/nvim-lspconfig'
Plug 'mfussenegger/nvim-jdtls'

Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

"Plug 'neoclide/coc.nvim', {'branch': 'release'}
"Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
"Plug 'chrisbra/Colorizer'

"THEMES
" Plug 'drewtempelmeyer/palenight.vim'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
"Plug 'ayu-theme/ayu-vim'

"Plug 'arcticicestudio/nord-vim'
"Plug 'morhetz/gruvbox'
"Plug 'sainnhe/gruvbox-material'
"Plug 'prettier/vim-prettier', { 'do': 'npm install' }
"Plug 'sainnhe/sonokai'
"Plug 'phanviet/vim-monokai-pro'
call plug#end()

"Emmet
let g:user_emmet_install_global = 0
autocmd FileType html,css EmmetInstall

"colorizer
let g:colorizer_auto_color = 1
let g:colorizer_auto_filetype='*'

"coc
"source ~/dotfiles/nvim/coc.vim

"----END PLUGIN MANAGEMENT----

"----THEME----
set background=dark
" colorscheme gruvbox
"let ayucolor="light"
"let ayucolor="mirage"
let ayucolor="dark"
colorscheme ayu

" let g:gruvbox_italicize_comments = 1
" let g:gruvbox_contrast_dark = 'hard'

"let g:airline_theme='base16_gruvbox_dark_hard'
let g:airline_theme='ayu'

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

let g:airline_power_fonts = 0
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#formatter = 'unique_tail'
let g:airline#extensions#tabline#enabled = 1

" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.whitespace = 'Ξ'

" airline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''
"----END THEME----



"----Key bindings----
let mapleader=" "
vmap < <gv
vmap > >gv

vnoremap <C-j> 5j
vnoremap <C-k> 5k
nnoremap <C-j> 5j
nnoremap <C-k> 5k

nnoremap <C-s> :split<CR>
nnoremap <A-s> :vsp<CR>

nnoremap <A-n> :tabnew<CR>
nnoremap <A-k> :tabnext<CR>
nnoremap <A-j> :tabprevious<CR>
nnoremap <A-c> :tabclose<CR>

nnoremap <leader>k :%bd <bar> e#

vnoremap <C-c> "+y
nnoremap <leader>l :noh<cr>

"FZF
" nnoremap <leader>ff :call fzf#run({'options': '--reverse --prompt "FZF:"', 'down': 20, 'dir': '.', 'sink': 'e' })<CR>
" nnoremap <leader>ff :Files<CR>
" nnoremap <leader>fg :GFiles<CR>

"Coc
" xmap <leader>f  <Plug>(coc-format-selected)
" nmap <leader>f  <Plug>(coc-format-selected)
" nmap <leader>rn <Plug>(coc-rename)

" nnoremap <leader><leader>d :call CocAction('jumpDefinition', 'tab new')<CR>
" nnoremap <leader>rw :CocSearch <C-R>=expand("<cword>")<CR><CR>
" nnoremap <leader>fw :CocSearch -S 
" nnoremap <leader>a :CocAction<CR>
" map <C-n> :CocCommand explorer<CR>

"----END bindings----



"----REMAPS----
nnoremap 0 ^
vnoremap <leader>p "_dP
vnoremap <leader>P "_dP

inoremap {<CR> {<CR>}<ESC>O
"----END REMAPS----



"----COMMANDS----
command! Confe execute "e ~/.config/nvim/init.vim"
command! Confr execute "source ~/.config/nvim/init.vim"
"command! FormatXML execute "%s/></>\r</g | :0 | =:$"
command! FormatXML execute "%s/></>\r</g"
command! FormatJSON execute "%!python -m json.tool"

"Removes trailing spaces
function TrimWhiteSpace()
  %s/\s*$//
  ''
endfunction
command! TrimWhiteSpace execute "call TrimWhiteSpace()"

command! Vifm execute "! tmux neww -a 'vifm %:h'"
"----END COMMANDS----



"----Vim configuration----
if (has("nvim"))
  let $NVIM_TUI_ENABLE_TRUE_COLOR=1
endif

if (has("termguicolors"))
  set termguicolors
endif

if exists('+termguicolors')
  let &t_8f = "\<Esc>[38;2%lu;%lu;%lum]"
  let &t_8b = "\<Esc>[48;2%lu;%lu;%lum]"
endif

lua <<EOF
require'nvim-treesitter.configs'.setup {
  highlight = {
    enable = true,
  },
}
EOF


