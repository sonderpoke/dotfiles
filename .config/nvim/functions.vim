" command! Vimrc execute "e ~/.config/nvim/init.vim"
" command! LoadVimrc execute "source ~/.config/nvim/init.vim"

"Removes trailing spaces
function TrimWhiteSpace()
  %s/\s*$//
endfunction
command! TrimWhiteSpace execute "call TrimWhiteSpace()"
" command! TrimWhiteSpace execute "%s/ *$//"

"  FOLDING
function Fold()
  set foldmethod=indent
  normal zR
endfunction
command! Fold execute "call Fold()"

"  FILE FORMATTING
command! FormatXML execute "%s/></>\r</g"
command! FormatJSON execute "%!python -m json.tool"
"command! FormatXML execute "%s/></>\r</g | :0 | =:$"

command! Vifm execute "! tmux neww -a 'vifm %:h'"

command! Date :normal a<C-R>=strftime('%Y-%m-%d')<CR>
command! DateTime :normal a<C-R>=strftime('%Y-%m-%d %H:%M:%S')<CR>

" command! WriteBranchName :execute "read !git branch --show-current | awk -F '/' '{print $2 \": \"}'" | execute 'normal kddA'

command! GenSerialVersionUID execute "lua require('java.serial-version-uid').generateSerialUID()"

command! WriteGitMessage execute "lua GitMessage()"

