local function file_exists(file)
  local f = io.open(file, "r")
  if f then f:close() end
  return f ~= nil
end

local function lines_from(file)
  if not file_exists(file) then return {} end
  local lines = {}
  for line in io.lines(file) do
    lines[#lines + 1] = line
  end
  return lines
end

local function getCredentials()
  local file = '/home/czr/work/.asseco'
  local lines = lines_from(file)

  local credentials = {}

  for k,v in pairs(lines) do
    if k == 1 then
      credentials.username = v:gsub("username=", "")
    end
    if k == 2 then
      credentials.password = v:gsub("jira=", "")
    end
    -- print('line[' .. k .. ']', v)
  end
  return credentials
end

function GitMessage()
  local credentials = getCredentials()

  local issueNumCmd = "git branch --show-current | awk -F '/' '{print $2}'"
  local issueNum = ""

  local handle = io.popen(issueNumCmd)
  local result = handle:read("*a")
  result = result:gsub("\r", "")
  result = result:gsub("\n", "")
  issueNum = result
  handle:close()

  local messageCmd = "curl -s -u "..credentials.username..":"..credentials.password
  messageCmd = messageCmd.." https://www.asseco.lt/jira/rest/api/2/issue/"..issueNum.." | jq '.fields.summary'"
  local message = " "
  -- local message = ": done -> "

  handle = io.popen(messageCmd)
  result = handle:read("*a")
  result = result:gsub("\r", "")
  result = result:gsub("\n", "")
  message = message..result
  -- message = message..result..";"
  handle:close()

  local row, col = unpack(vim.api.nvim_win_get_cursor(0))
  local buf = vim.api.nvim_get_current_buf()
  local lines = {issueNum..message}

  -- print(lines[1])

  vim.api.nvim_buf_set_lines(buf, row-1, row, true, lines)
end
