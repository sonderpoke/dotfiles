vim.cmd('syntax on') -- syntax highlighting
-- vim.cmd('set iskeyword-=_') -- treat dash separated words as a word text object"
-- vim.cmd('set listchars+=space:.')
vim.cmd('set clipboard+=unnamedplus') -- Copy paste between vim and everything else
vim.cmd('set list')
vim.cmd [[ filetype plugin indent on ]] -- indentation based on filetype
vim.cmd('set colorcolumn=140')

-- vim.cmd('e ++ff=unix') // delete windows spaces

-- vim.o.foldmethod="indent"
-- vim.o.foldlevelstart=20
-- vim.o.foldlevel=20
-- vim.o.foldnestmax=20
-- vim.cmd [[ autocmd Syntax java,ts,html setlocal foldmethod=syntax ]]
-- vim.cmd [[ autocmd Syntax java,ts,html normal zR ]]


vim.wo.spell = false

vim.bo.sw=2
vim.bo.ts=2
vim.bo.smartindent = true -- Makes indenting smart
vim.bo.expandtab = true
vim.bo.autoindent = true
vim.bo.swapfile = false
-- vim.bo.grepprg = 'rg'

vim.o.encoding = "utf-8"
vim.o.fileencoding = "utf-8" -- The encoding written to file
vim.o.scrolloff = 5
vim.o.wrap=false
vim.o.cmdheight = 2 -- More space for displaying messages

--vim.o.t_Co = "256" -- Support 256 colors
vim.o.title = true

vim.wo.number = true -- set numbered lines
vim.wo.relativenumber = true -- set relative number
vim.wo.cursorline = true -- Enable highlighting of the current line
vim.wo.signcolumn = "yes" -- Always show the signcolumn, otherwise it would shift the text each time

vim.o.showtabline = 2 -- Always show tabs

vim.o.splitright = false -- Vertical splits will automatically be to the right
vim.o.splitbelow = false -- Horizontal splits will automatically be below

vim.o.termguicolors = true -- set term gui colors most terminals support this
vim.o.mouse = "a" -- Enable your mouse
vim.o.mousefocus = true

vim.o.conceallevel = 0 -- So that I can see `` in markdown files
vim.o.backup = false -- This is recommended by coc
vim.o.writebackup = false -- This is recommended by coc

vim.o.updatetime = 300 -- Faster completion
vim.o.timeoutlen = 300 -- By default timeoutlen is 1000 ms

--vim.o.colorcolumn = 140

-- vim.o.guifont = "Fira Code:h17"

vim.o.laststatus=3
