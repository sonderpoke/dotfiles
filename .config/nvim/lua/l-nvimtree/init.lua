vim.g.nvim_tree_refresh_wait = 500 -- "1000 by default, control how often the tree can be refreshed, 1000 means the tree can be refresh once per 1000ms.
vim.api.nvim_set_keymap('n', '<C-n>', ':NvimTreeToggle<CR>', {noremap = true, silent = true})
vim.api.nvim_set_keymap('n', '<leader>n', ':NvimTreeFindFile<CR>', {noremap = true, silent = true})
vim.cmd[[highlight NvimTreeFolderIcon guibg=none]]

require'l-nvimtree/nvimtree-options'.setupNvimTree()
