vim.o.completeopt = "menuone,noselect"

local cmp = require'cmp'

local luasnip = require('luasnip')
-- luasnip.filetype_extend("typescript", {"javascript"})

require('luasnip.loaders.from_vscode').lazy_load()

local has_words_before = function()
  local line, col = unpack(vim.api.nvim_win_get_cursor(0))
  return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

local lsp = {
  kinds = {
    Text = '',
    Method = '',
    Function = '',
    Constructor = '',
    Field = 'ﰠ',
    Variable = '',
    Class = 'ﴯ',
    Interface = '',
    Module = '',
    Property = 'ﰠ',
    Unit = '塞',
    Value = '',
    Enum = '',
    Keyword = '',
    Snippet = '',
    Color = '',
    File = '',
    Reference = '',
    Folder = '',
    EnumMember = '',
    Constant = '',
    Struct = 'פּ',
    Event = '',
    Operator = '',
    TypeParameter = '',
  },
}

  local lspkind_comparator = function(conf)
    local lsp_types = require('cmp.types').lsp
    return function(entry1, entry2)
      -- if entry1.source.name ~= 'nvim_lsp' then
      --   if entry2.source.name == 'nvim_lsp' then
      --     return false
      --   else
      --     return nil
      --   end
      -- end
      local kind1 = lsp_types.CompletionItemKind[entry1:get_kind()]
      local kind2 = lsp_types.CompletionItemKind[entry2:get_kind()]

      local priority1 = conf.kind_priority[kind1] or 0
      local priority2 = conf.kind_priority[kind2] or 0
      if priority1 == priority2 then
        return nil
      end
      return priority2 < priority1
    end
  end

  local label_comparator = function(entry1, entry2)
    return entry1.completion_item.label < entry2.completion_item.label
  end

cmp.setup({
  mapping = {
    ['<C-d>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-e>'] = cmp.mapping.close(),
    ['<CR>'] = cmp.mapping.confirm({ select = true }),

    ['<Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      elseif luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
      elseif has_words_before() then
        fallback()
      else
        fallback()
      end
    end, { "i", "s" }),
    ['<Down>'] = cmp.mapping(cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Select }), {'i'}),
    ['<Up>'] = cmp.mapping(cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Select }), {'i'}),
    ['<S-Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif luasnip.jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end, { "i", "s" }),

  },
  sources = {
    { name = 'nvim_lsp' },
    -- { name = 'vsnip', keyword_length = 1},
    -- { name = 'luasnip', keyword_length = 1, max_item_count = 10},
    { name = 'luasnip' },
    { name = 'path', keyword_length = 2},
    { name = 'buffer', keyword_length = 3, max_item_count = 30 },
    -- { name = "spell", keyword_length = 2, max_item_count = 20 },

    -- { name = 'vsnip' },
    -- { name = 'nvim_lsp' },
    -- { name = 'buffer' },
    -- { name = 'path' },
    -- { name = 'spell' },

    -- { name = 'vsnip', score = 4 },
    -- { name = 'nvim_lsp', score = 3 },
    -- { name = 'buffer', score = 2 },
    -- { name = 'path', score = 1 },
    -- { name = 'spell', score = 0 },
  },
  -- snippet = {
  --   expand = function(args)
  --     vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` user.
  --   end,
  -- },
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)
    end,
  },
  formatting = {
    format = function(_, vim_item)
      vim_item.kind = string.format(
      '%s [%s]',
      lsp.kinds[vim_item.kind],
      vim_item.kind
      )
      return vim_item
    end,
  },
  experimental = {
    ghost_text = true
  }
  -- sorting = {
  --   comparators = {
  --     lspkind_comparator({
  --       kind_priority = {
  --         Snippet = 12,
  --         Field = 11,
  --         Property = 11,
  --         Constant = 10,
  --         Enum = 10,
  --         EnumMember = 10,
  --         Event = 10,
  --         Function = 10,
  --         Method = 10,
  --         Operator = 10,
  --         Reference = 10,
  --         Struct = 10,
  --         Variable = 9,
  --         File = 8,
  --         Folder = 8,
  --         Class = 5,
  --         Color = 5,
  --         Module = 5,
  --         Keyword = 2,
  --         Constructor = 1,
  --         Interface = 1,
  --         Text = 1,
  --         TypeParameter = 1,
  --         Unit = 1,
  --         Value = 1,
  --       },
  --     }),
  --     label_comparator,
  --   },
  -- }
})

-- require'compe'.setup {
--   enabled = true;
--   autocomplete = true;
--   debug = false;
--   min_length = 1;
--   preselect = 'enable';
--   throttle_time = 80;
--   source_timeout = 200;
--   incomplete_delay = 400;
--   max_abbr_width = 100;
--   max_kind_width = 100;
--   max_menu_width = 100;
--   documentation = true;

--   source = {
--     path = true;
--     buffer = true;
--     -- calc = true;
--     nvim_lsp = true;
--     -- nvim_lua = true;
--     vsnip = true;
--     -- emoji = true;
--     spell = false;
--   };

  -- source = {
  --   path = {kind = "   (Path)"},
  --   buffer = {kind = "   (Buffer)"},
  --   calc = {kind = "   (Calc)"},
  --   vsnip = {kind = "   (Snippet)"},
  --   nvim_lsp = {kind = "   (LSP)"},
  --   -- nvim_lua = {kind = "  "},
  --   nvim_lua = false,
  --   spell = {kind = "   (Spell)"},
  --   tags = false,
  --   vim_dadbod_completion = true,
  --   -- snippets_nvim = {kind = "  "},
  --   -- ultisnips = {kind = "  "},
  --   -- treesitter = {kind = "  "},
  --   emoji = {kind = " ﲃ  (Emoji)", filetypes={"markdown", "text"}}
  --   -- for emoji press : (idk if that in compe tho)
  -- }


-- }


-- local t = function(str)
--   return vim.api.nvim_replace_termcodes(str, true, true, true)
-- end
-- 
-- local check_back_space = function()
--   local col = vim.fn.col('.') - 1
--   if col == 0 or vim.fn.getline('.'):sub(col, col):match('%s') then
--     return true
--   else
--     return false
--   end
-- end
-- 
-- -- Use (s-)tab to:
-- --- move to prev/next item in completion menuone
-- --- jump to prev/next snippet's placeholder
-- _G.tab_complete = function()
--   if vim.fn.pumvisible() == 1 then
--     return t "<C-n>"
--   elseif vim.fn.call("vsnip#available", {1}) == 1 then
--     return t "<Plug>(vsnip-expand-or-jump)"
--   elseif check_back_space() then
--     return t "<Tab>"
--   else
--     return vim.fn['compe#complete']()
--   end
-- end
-- _G.s_tab_complete = function()
--   if vim.fn.pumvisible() == 1 then
--     return t "<C-p>"
--   elseif vim.fn.call("vsnip#jumpable", {-1}) == 1 then
--     return t "<Plug>(vsnip-jump-prev)"
--   else
--     return t "<S-Tab>"
--   end
-- end
-- 
-- vim.api.nvim_set_keymap("i", "<Tab>", "v:lua.tab_complete()", {expr = true})
-- vim.api.nvim_set_keymap("s", "<Tab>", "v:lua.tab_complete()", {expr = true})
-- vim.api.nvim_set_keymap("i", "<S-Tab>", "v:lua.s_tab_complete()", {expr = true})
-- vim.api.nvim_set_keymap("s", "<S-Tab>", "v:lua.s_tab_complete()", {expr = true})
-- 
-- -- vim.api.nvim_set_keymap('i', '<CR>', "vim.fn['compe#complete']()", {silent = true, expr = true})
-- vim.api.nvim_set_keymap('i', '<CR>', "v:lua.compe_complete()", {silent = true, expr = true})
-- 
-- vim.cmd[[ inoremap <silent><expr> <CR> compe#confirm('<CR>') ]]
-- vim.cmd[[ inoremap <silent><expr> <C-Space> compe#complete() ]]
-- 
-- 
-- 
