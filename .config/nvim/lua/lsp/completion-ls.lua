-- require'lspconfig'.pyls.setup{on_attach=require'completion'.on_attach}
-- vim.o.completion_confirm_key = "<CR>"

vim.api.nvim_set_var('completion_trigger_keyword_length', 3)
vim.api.nvim_set_var('completion_timer_cycle', 1000)

vim.api.nvim_set_var('completion_enable_auto_signature', 0)
-- vim.api.nvim_set_var('completion_trigger_keyword_length', '.')

