local treesitter = vim.treesitter
local ts_utils = require('nvim-treesitter.ts_utils')
-- local parsers = require('nvim-treesttter.parsers')
local locals = require('nvim-treesitter.locals')
local ts_query = require('nvim-treesitter.query')


local pickers = require "telescope.pickers"
local finders = require "telescope.finders"
local conf = require("telescope.config").values

local actions = require "telescope.actions"
local action_state = require "telescope.actions.state"

local bufnr = vim.api.nvim_get_current_buf()
local filetype = vim.api.nvim_buf_get_option(bufnr, 'ft')

local methods = function(opts, results)
  pickers.new(opts, {
    previewer = require "telescope.previewers".vimgrep.new({
      title = 'title',
      treesitter = true,
      cwd = vim.fn.getcwd(),
      -- highlighter = require('telescope.previewers.utils').ts_highlighter(vim.api.nvim_get_current_buf(), filetype),
    }),
    prompt_title = "methods",
    finder = finders.new_table {
      results = results,
      entry_maker = function(entry)
        return {
          value = entry,
          display = entry[1],
          ordinal = entry[1],
          filename = vim.fn.expand('%'),
          lnum = entry[2],
          filetype = filetype
        }
      end
    },
    sorter = conf.generic_sorter(opts),
    attach_mappings = function(prompt_bufnr, map)
      local reload_package = function()
        local selection = action_state.get_selected_entry()

        actions.close(prompt_bufnr)

        -- print(vim.inspect(selection))
        -- print(vim.inspect(selection.value))
        -- print()

        vim.cmd(':'..selection.value[2])
        vim.cmd(':norm! zz')


      end

      map("i", "<CR>", reload_package)
      map("n", "<CR>", reload_package)

      return true
    end
  }):find()
end

local function findTypeMaxLen(results, first, second)
  local longest = 0
  for key, value in pairs(results) do
    local type_len = string.len(value[first][second])
    if type_len > longest then
      longest = type_len
    end
  end
  return longest
end

local function formatResults(results)
  local new_results = {}

  for key, value in pairs(results) do
    local old_method, line_nr = unpack(value)

    local type, name, params = unpack(old_method)

    local type_max_len = findTypeMaxLen(results, 1, 1)
    local type_len = string.len(type)
    if type_len < type_max_len then
      type = type..string.rep(' ', type_max_len - type_len)
    end

    local name_max_len = findTypeMaxLen(results, 1, 1)
    local name_len = string.len(name)
    if name_len < name_max_len then
      name = name..string.rep(' ', name_max_len - name_len)
    end

    local method = type..'    '..name..params
    -- table.insert(new_results, key, {method, line_nr})
    table.insert(new_results, 1, {method, line_nr})
  end

  return new_results
end

local M = {}

function M.test()


--------------------------------------- JAVA ---------------------------------------
  local java_query = [[
  (method_declaration
    (modifiers) @f.modifiers
    type: (_) @f.type
    name: (_) @f.name
    parameters: (_) @f.params)
  ]]
------------------------------------------------------------------------------------


------------------------------------ TYPESCRIPT ------------------------------------
  local typescript_query = [[
  (method_definition
    (accessibility_modifier)? @f.modifiers
    name: (_) @f.name
    parameters: (_) @f.params
    return_type: (_)? @f.type)
  ]]
------------------------------------------------------------------------------------


--------------------------------------- LUA ----------------------------------------
  local lua_query = [[
  (function (function_name) @function.name)
  ]]
  -- local lua_query = [[
  -- (function (function_name) @function.name)
  -- ]]
------------------------------------------------------------------------------------


  -- local supported_ft = {
  --   java = true,
  --   typescript = true
  -- }

  local success, parsed_query = pcall(function()
    local query = java_query
    if filetype == 'java' then
      query = java_query
    end
    if filetype == 'typescript' then
      query = typescript_query
    end

    return treesitter.parse_query(filetype, query)
  end)

  if not success then
    print('Not a success')
    return
  end

  local parser = vim.treesitter.get_parser(bufnr, filetype)

  local root = parser:parse()[1]:root()

  local start_row, _, end_row, _ = root:range()

  local results = {}

  local match_counter = 1
  for match in ts_query.iter_prepared_matches(parsed_query, root, bufnr, start_row, end_row) do

    local method = {}
    local fn_modifiers = ""
    local fn_type = ""
    local fn_name = ""
    local fn_params = ""
    local row = 0

    locals.recurse_local_nodes(match, function(_, node, path)
      if path == "f.modifiers" then
        fn_modifiers = ts_utils.get_node_text(node)[1]
      end
      if path == "f.type" then
        fn_type = ts_utils.get_node_text(node)[1]
      end
      if path == "f.name" then
        fn_name = ts_utils.get_node_text(node)[1]
        row = ts_utils.get_node_range(node) + 1
      end
      if path == "f.params" then
        fn_params = ts_utils.get_node_text(node)[1]
      end

      -- if fn_name ~= "" and fn_type ~= "" then
      -- if fn_name ~= "" and fn_params ~= "" then
      -- end

    end)

    method = {fn_modifiers..' '..fn_type, fn_name, fn_params}
    -- method = fn_modifiers..' '..fn_type..' '..fn_name..' '..fn_params
    table.insert(results, match_counter, {method, row})
    match_counter = match_counter + 1

  end

  results = formatResults(results)


  -- local opts = require("telescope.themes").get_dropdown{}
  local opts = {}
  methods(opts, results)

end

return M
