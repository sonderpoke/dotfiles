#----------------------------------- PLUGINS ------------------------------------#
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=246,underline'
bindkey '^ ' autosuggest-accept

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
#--------------------------------------------------------------------------------#


#------------------------------------ THEME -------------------------------------#
source $HOME/.config/zsh/agnoster-zsh-theme/agnoster.zsh-theme
setopt prompt_subst
#--------------------------------------------------------------------------------#


#---------------------------------- COMPLETION ----------------------------------#
autoload -Uz compinit
compinit
zstyle ':completion:*' menu select
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
setopt COMPLETE_ALIASES
#zstyle ':completion::complete*' gain-privileges 1
#--------------------------------------------------------------------------------#


#------------------------------------- FZF --------------------------------------#
source /usr/share/fzf/completion.zsh
source /usr/share/fzf/key-bindings.zsh
#--------------------------------------------------------------------------------#


#------------------------------------ LOCALE ------------------------------------#
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8
#--------------------------------------------------------------------------------#


#----------------------------------- HISTORY ------------------------------------#
# HISTFILE=~/.histfile
HISTFILE=~/.zhistory
HISTSIZE=20000
SAVEHIST=20000
#--------------------------------------------------------------------------------#


#----------------------------------- VIM MODE -----------------------------------#
set -o vi
bindkey -v
bindkey '^K' up-history
bindkey '^J' down-history
#--------------------------------------------------------------------------------#


#----------------------------------- ALIASES ------------------------------------#
#alias mkdir='mkdir -p'

alias grep='grep --color=auto --exclude-dir={.git,.svn}'

# navigation
alias ..='cd ..'
alias ...='cd ../..'
alias cdd='cd ~/Downloads'
alias cdD='cd ~/Desktop'
alias cd.='cd ~/dotfiles'
alias cdw='cd ~/work'
alias cdwi='cd ~/work/init'

alias p='python3'

alias nvim=/usr/local/bin/nvim
alias vim=nvim
alias v=nvim
alias tmux='tmux -u'

### work related
alias epp_update='~/work/init/epp_update_catalina.sh'
alias cdepp='cd ~/work/epp'
alias cdrepo='cd ~/work/EAIS2/repo'
alias gomux='~/.config/tmux/gomux.sh'
alias addmux='~/.config/tmux/addmux.sh'
alias workinit='~/work/init/work_init.sh'
alias start_work_db='~/work/KCIS2/dev_env/postgres/start.sh'
alias start_work_proxy='node ~/work/KCIS2/dev_env/proxy_server/kcis_snapshot.js'
alias stop_work_db='~/work/KCIS2/dev_env/postgres/stop.sh'

### PERSONAL
alias cdp='cd ~/personal/'
alias personalinit='~/personal/init/personal_init.sh'

#Git
alias gl='git log --oneline --graph --decorate --color=always'
alias gla='git log --oneline --graph --decorate --color=always --abbrev-commit --all'
alias gs='git status'
alias gp='git pull'
alias gpo='git push origin main'
alias gpom='git push origin master'
alias gd='git diff HEAD'
alias ga='git add --all'
alias gc='git commit -a'
alias gca='git commit --amend'

# wttr
# alias weather='curl wttr.in/Vilnius'
alias weather='curl -s v2d.wttr.in/Vilnius | head -43'
alias moon='curl -s wttr.in/Moon | head -25' 

#maven
alias maven-settings='~/.config/dmenu_scripts/set_mvn_settings.sh'

### GAMES
alias gamerun='~/.config/dmenu_scripts/games.sh'


alias ls='exa -GF --color=always --icons -x --group-directories-first'
alias la='exa -laF --color=always --icons -x --group-directories-first'
#--------------------------------------------------------------------------------#


#---------------------------------- FUNCTIONS -----------------------------------#
function changeTitle {
  echo -e "\e]2;Alacritty [$(pwd)]\e\\"
}

# function launchvim {
#   echo -e "\e]2;Neovim [$(pwd)]\e\\"; nvim "$@"; changeTitle
# }

function cd {
  builtin cd "$@" && ls -F && changeTitle
}
#--------------------------------------------------------------------------------#


source /etc/environment
