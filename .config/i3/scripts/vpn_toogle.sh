#!/bin/bash

result=""

if [[ $button -eq 1 ]];
then
  echo starting
  $(~/vpn.sh > /dev/null)

  while true; do
    if [[ $result != "" ]]; then
      break
    fi
    result=$(ip addr | grep ppp0 | grep inet | awk -F ' ' '{print $1}')
    echo result
    sleep 1
  done
  # $(sleep 3; ~/resolve.sh > /dev/null)
  # $(sleep 3; ~/add_routes.sh > /dev/null)
  $(~/add_routes.sh > /dev/null)
elif [[ $button -eq 2 ]];
then
  echo stopping
  $(sudo killall openfortivpn)
else
  echo $(~/.config/i3/scripts/vpnip.sh)
fi
