#!/bin/bash

#interface=enp3s0
old_path="$HOME/.config/i3/scripts/temp/old"

read rx < "/sys/class/net/${interface}/statistics/rx_bytes"
read tx < "/sys/class/net/${interface}/statistics/tx_bytes"

read old_data < "$old_path"
old=(${old_data//;/ })

rx_diff=$(($rx - ${old[0]}))
tx_diff=$(($tx - ${old[1]}))


echo "${rx} ${tx}" > $old_path

rx_unit=b
tx_unit=b

if [[ $tx_diff -gt 1048576 ]]; then
  tx_diff=$(($tx_diff / 1024 / 1024))
  tx_unit=M
elif [[ $tx_diff -gt 1023 ]]; then
  tx_diff=$(($tx_diff / 1024))
  tx_unit=K
fi

if [[ $rx_diff -gt 1048576 ]]; then
  rx_diff=$(($rx_diff / 1024 / 1024))
  rx_unit=M
elif [[ $rx_diff -gt 1023 ]]; then
  rx_diff=$(($rx_diff / 1024))
  rx_unit=K
fi

echo " ${rx_diff}${rx_unit}  ${tx_diff}${tx_unit}"

