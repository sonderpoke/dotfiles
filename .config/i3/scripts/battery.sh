#!/bin/bash
status=$(cat /sys/class/power_supply/BAT0/uevent | awk '/STATUS/ {split($0,a,"="); print a[2]}')
full=$(cat /sys/class/power_supply/BAT0/energy_full)
now=$(cat /sys/class/power_supply/BAT0/energy_now)

percent=$(echo "$full $now" | awk '{printf("%.0f"), ($2/$1 * 100) }')

size=medium

if [[ "$percent" -gt 89 ]]; then
  color="#A1FF00"
  battery=""
elif [[ "$percent" -gt 79 ]]; then
  color="#b8FF00"
  battery=""
elif [[ "$percent" -gt 69 ]]; then
  color="#EEFF00"
  battery=""
elif [[ "$percent" -gt 59 ]]; then
  color="#FFFD00"
  battery=""
elif [[ "$percent" -gt 49 ]]; then
  color="#FFDC00"
  battery=""
elif [[ "$percent" -gt 39 ]]; then
  color="#FFBB00"
  battery=""
elif [[ "$percent" -gt 29 ]]; then
  color="#FFAA00"
  battery=""
elif [[ "$percent" -gt 19 ]]; then
  color="#FF7700"
  battery=""
else
  #low=true
  color="#FF0000"
  battery=""
  size=large
fi

if [[ ! "percent" -gt 5 ]]; then
  low=true
fi

if [[ $status == "Charging" ]]; then
  battery=""
  echo "<span color=\"$color\" size=\"$size\">$battery $percent%</span>"
else
  if [[ $low == "true" ]]; then
    echo "<span color=\"$color\" size=\"$size\">$battery $percent%</span>"
    notify-send "Battery is very low" && sleep 10
    echo "Battery is low" | festival --tts
  else
    echo "<span color=\"$color\" size=\"$size\">$battery $percent%</span>"
  fi
fi


