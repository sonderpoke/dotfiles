#!/bin/bash
action=$(echo -e "Cancel\nReboot\nLog out\nSuspend\nPower off" | dmenu -i -fn 'Hack-15' -nb '#333333' -nf '#00FF00' -sb '#afdafd' -sf '#000000' -p "$1")
if [ "$action" == "Reboot" ]; then
  systemctl reboot
elif [ "$action" == "Log out" ]; then
  i3-msg exit
elif [ "$action" == "Suspend" ]; then
  systemctl suspend
elif [ "$action" == "Power off" ]; then
  systemctl poweroff
fi
