#!/bin/bash
if [ "$button" == "1" ]; then
  ip=$(ip addr show dev wlp4s0 | grep 'inet ' | awk '{split($2, arr, "/"); print arr[1]}')
  notify-send "ip" "$ip"
  strength=$(iwconfig wlp4s0 | grep Quality | sed 's/[a-zA-Z=]//g' | awk '{split($1, arr, "/"); printf "%.2s", (arr[1] * 100 / 70) }')
  echo "直  $strength%"
else
  strength=$(iwconfig wlp4s0 | grep Quality | sed 's/[a-zA-Z=]//g' | awk '{split($1, arr, "/"); printf "%.2s", (arr[1] * 100 / 70) }')
  echo "直  $strength%"
fi
