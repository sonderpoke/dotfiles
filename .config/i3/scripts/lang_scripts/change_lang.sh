#!/bin/bash
if [[ $button -eq 1 ]]; then
  if [[ $(~/.config/i3/scripts/lang_scripts/get_lang.sh) == "US" ]]; then
    $(~/.config/i3/scripts/lang_scripts/lt.sh)
    echo "<span size=\"large\"> </span>LT"
  else
    $(~/.config/i3/scripts/lang_scripts/us.sh)
    echo "<span size=\"large\"> </span>US"
  fi
else
  echo "<span size=\"large\"> </span>$(~/.config/i3/scripts/lang_scripts/get_lang.sh)"
fi
