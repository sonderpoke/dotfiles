#!/bin/bash
if [[ $button -eq 1 ]]; then
  result=$(spt playback -n)
elif [[ $button -eq 2 ]]; then
  result=$(spt playback -t)
elif [[ $button -eq 3 ]]; then
  result=$(spt playback -p)
else
  result=$(spt playback -s)
fi

if [[ "$result" != *"⏸"* ]] || [ "$result" = "" ]; then
  result=$(echo $result | cut -c 5-)
else
  result=" paused"
fi

echo "<span> ${result:0:40} </span>"
