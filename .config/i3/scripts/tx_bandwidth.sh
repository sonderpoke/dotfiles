#!/bin/bash
vpn_int=$(ip route | awk '/ppp0/ { print $3 ; exit }')
if [[ ! $vpn_int == "ppp0" ]]; then
  interface=$(ip route | awk '/^default/ { print $5 ; exit }')
else
  interface=$vpn_int
fi

old_path="$HOME/.config/i3/scripts/temp/old_tx"

read tx < "/sys/class/net/${interface}/statistics/tx_bytes"
rx=0

read old_data < "$old_path"
old=(${old_data//;/ })

tx_diff=$(($tx - ${old[1]}))

echo "${rx} ${tx}" > $old_path

tx_unit=b

if [[ $tx_diff -gt 1048576 ]]; then
  tx_diff=$(($tx_diff / 1024 / 1024))
  tx_unit=M
elif [[ $tx_diff -gt 1023 ]]; then
  tx_diff=$(($tx_diff / 1024))
  tx_unit=K
fi

echo "<span size=\"small\">${tx_diff}${tx_unit}</span>"
