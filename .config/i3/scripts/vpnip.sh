#!/bin/bash
ppp0=$(ip addr show primary | grep ppp0)
if [[ ! "$ppp0" == "" ]] ; then
  echo $(ip addr show dev ppp0 | grep 'inet ' | awk '{split($2, arr, "/"); print "ppp0:"arr[1]}')
else
  echo "<span color=\"#fb4934\" size=\"large\">vpn</span>"
fi
