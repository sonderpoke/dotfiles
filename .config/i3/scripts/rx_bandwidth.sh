#!/bin/bash
vpn_int=$(ip route | awk '/ppp0/ { print $3 ; exit }')
if [[ ! $vpn_int == "ppp0" ]]; then
  interface=$(ip route | awk '/^default/ { print $5 ; exit }')
else
  interface=$vpn_int
fi

old_path="$HOME/.config/i3/scripts/temp/old_rx"

read rx < "/sys/class/net/${interface}/statistics/rx_bytes"
tx=0

read old_data < "$old_path"
old=(${old_data//;/ })

rx_diff=$(($rx - ${old[0]}))

echo "${rx} ${tx}" > $old_path

rx_unit=b

if [[ $rx_diff -gt 1048576 ]]; then
  rx_diff=$(($rx_diff / 1024 / 1024))
  rx_unit=M
elif [[ $rx_diff -gt 1023 ]]; then
  rx_diff=$(($rx_diff / 1024))
  rx_unit=K
fi

echo "<span size=\"small\">${rx_diff}${rx_unit}</span>"
