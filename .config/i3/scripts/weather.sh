#!/bin/bash
weather=$(curl -s "wttr.in/Vilnius?format=3" | cut -c 10-)
w_icon=$(echo $weather | awk '{printf $1}')
w_tmp=$(echo $weather | awk '{printf $2}')

if [ "$button" == "1" ]; then
  #notify-send "Weather" "$(curl -s v2d.wttr.in/Vilnius | tail -n 6 | head -n 5)"
  #notify-send "Weather" "$( curl -s "wttr.in/Vilnius?format=4" | cut -c 10-)"
  notify-send "Weather" "$(curl -s v2d.wttr.in/Vilnius | tail -n 7 | head -n 1 | sed -e 's/\[2m//' -e 's/\[0m//' -e 's/\x1b//g')"
  echo "<span size=\"x-large\">$w_icon</span>$w_tmp"
else
  echo "<span size=\"x-large\">$w_icon</span>$w_tmp"
fi
